# Entgra Icons

This repos contains two projects,

1. Entgra Icons

    Font Entgra gives you scalable vector icons that can instantly customized and use on any website with the power of CSS.

    [Read More](entgra-icons/README.md)

2. Entgra Icons for React

    This is a react wrapper for Enntgra icons
    
    [Read More](entgra-icons-react/README.md)

## License

Entgra.io. licenses this source under the Apache License, Version 2.0 ([LICENSE](LICENSE)), You may not use this file except in compliance with the License.
