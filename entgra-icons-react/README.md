# Entgra Icons for React

This is a react wrapper for Entgra icons

## How to use?

### Installation

1. Since this npm package is hosted with [Entgra's npm registry](http://nexus.entgra.io/repository/npm-group/); you have to change your project's default repository
    
    Create a file named `.npmrc`
    ```
        nano .npmrc
    ```
   Add the following line to the file and save it
   ```
        registry=http://nexus.entgra.io/repository/npm-group/
   ```

2. Install the latest package with npm
   ```
        npm install entgra-icons-react
   ```

### Usage
```javascript
import ReactDOM from 'react-dom';
import { EntgraIcon } from 'font-entgra-react';
 
ReactDOM.render(<EntgraIcon icon="fw-image" />, document.body);
```

## How to Contribute?

### Adding new features
   Install Dependencies using `npm install`
   
   All the source files are located inside the `/src` directory 

### Building the package
    
   Run  ``npm run build`` to build the package 
   
### Publishing the package
   Currently we publish this package using (Entgra's nexus Repository Manager)[http://nexus.entgra.io/]
   
   1. Build the package
   2. Remove `node_modules` directory
        ```
          rm -rf node_modules
        ``` 
   3. Compress the package as a `.tgz` file  
      ```
        cd ../
        tar -cvzf entgra-icons-react.tgz entgra-icons-react
      ```
   4. Login to the (Entgra's nexus Repository Manager)[http://nexus.entgra.io/]
   5. Navigate to Uploads > npm-releases
   6. Upload the compressed package 
  

## License

Entgra.io licenses this source under the Apache License, Version 2.0 ([LICENSE](LICENSE)), You may not use this file except in compliance with the License.
