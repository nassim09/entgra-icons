import React from 'react';
import 'entgra-icons/dist/css/font-entgra.min.css';

const EntgraIcon= (props) => {
    return (
       <i className={'fw '+ props.icon} {...props}/>
    );
};

export default EntgraIcon;
